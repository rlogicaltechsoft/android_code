package com.rlogical.restaurant.api;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.rlogical.restaurant.Util.ConfigMain;

public class LoginApi{
	
	// Login API
	public LoginApi(){}
	
	public String login(String password) 
	{
		try 
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(ConfigMain.SERVERIP + "/Login?"
					+ "password=" + password);
			HttpResponse response = httpclient.execute(httpget);
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e) 
		{
			return null;
		}
		
	}
}
