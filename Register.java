package com.rlogical.homedelivery;

import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rlogical.restaurant.Util.ConfigMain;
import com.rlogical.restaurant.Util.Utility;
import com.rlogical.restaurant.api.CallRegistrationApi;

public class Register extends Activity {

	Context ctx;

	LinearLayout ll_reg_center;
	LinearLayout.LayoutParams parms;

	TextView lbl_register_title;
	Typeface RegisterTypeface;

	Button btn_Register;

	EditText txt_register_name, txt_register_email, txt_register_password,
			txt_register_confirmpassword, txt_register_postalcode,
			txt_register_tel, txt_register_address, txt_register_username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.register);
		
		Utility.hideKeyboard(Register.this);

		txt_register_name = (EditText) findViewById(R.id.txt_register_name);
		txt_register_email = (EditText) findViewById(R.id.txt_register_email);
		txt_register_password = (EditText) findViewById(R.id.txt_register_password);
		txt_register_confirmpassword = (EditText) findViewById(R.id.txt_register_confirmpassword);
		txt_register_postalcode = (EditText) findViewById(R.id.txt_register_postalcode);
		txt_register_tel = (EditText) findViewById(R.id.txt_register_tel);
		txt_register_address = (EditText) findViewById(R.id.txt_register_address);
		txt_register_username = (EditText) findViewById(R.id.txt_register_username);

		RegisterTypeface = Typeface.createFromAsset(getAssets(),
				"GurmukhiMN.ttc");

		txt_register_name.setTypeface(RegisterTypeface);
		txt_register_email.setTypeface(RegisterTypeface);
		txt_register_password.setTypeface(RegisterTypeface);
		txt_register_confirmpassword.setTypeface(RegisterTypeface);
		txt_register_postalcode.setTypeface(RegisterTypeface);
		txt_register_tel.setTypeface(RegisterTypeface);
		txt_register_address.setTypeface(RegisterTypeface);
		txt_register_username.setTypeface(RegisterTypeface);

		btn_Register = (Button) findViewById(R.id.btn_Register);
		btn_Register.setTypeface(RegisterTypeface);

		lbl_register_title = (TextView) findViewById(R.id.lbl_register_title);
		lbl_register_title.setTypeface(RegisterTypeface);
		ctx = this;

		parms = new LinearLayout.LayoutParams(
				(int) (Utility.getDeviceWidth(this) / 1.2),
				LayoutParams.WRAP_CONTENT);

		ll_reg_center = (LinearLayout) findViewById(R.id.ll_reg_center);
		ll_reg_center.setLayoutParams(parms);

		btn_Register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (txt_register_name.getText().toString().length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Name",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_email.getText().toString().length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Name",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_password.getText().toString().length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Password",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_confirmpassword.getText().toString()
						.length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Confirm Password",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_postalcode.getText().toString()
						.length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Postal code",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_tel.getText().toString().length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Telephone Number",
							Toast.LENGTH_SHORT).show();
				} else if (txt_register_address.getText().toString().length() == 0) {
					Toast.makeText(getBaseContext(), "Enter Address",
							Toast.LENGTH_SHORT).show();
				} else {
					if (!Utility.isEmail(txt_register_email.getText()
							.toString())) {
						Toast.makeText(getBaseContext(), "Enter Valid email",
								Toast.LENGTH_SHORT).show();
					} else if (!txt_register_password
							.getText()
							.toString()
							.equals(txt_register_confirmpassword.getText()
									.toString())) {
						Toast.makeText(getBaseContext(),
								"password and confirmpassword must be same",
								Toast.LENGTH_SHORT).show();
					} else {
						
						new AsyncRegistation(txt_register_name.getText().toString().trim().replaceAll(" ", "_"),
								txt_register_address.getText().toString().trim().replaceAll(" ", "_"),
								txt_register_tel.getText().toString(),
								txt_register_postalcode.getText().toString().replaceAll(" ", "_"), 
								txt_register_username.getText().toString(), 
								txt_register_password.getText().toString().trim(),
								txt_register_email.getText().toString().trim())
								.execute();
					}
				}
			}
		});
	}

	public class AsyncRegistation extends AsyncTask<Void, Void, String> {
		CustomProgressDialog dialog;

		String name, address, phoneno, postcode, username, password, email;

		public AsyncRegistation(String nm, String add, String phno,
				String pcode, String unm, String pass, String mail) {
			this.name = nm;
			this.address = add;
			this.phoneno = phno;
			this.postcode = pcode;
			this.username = unm;
			this.password = pass;
			this.email = mail;

			dialog = new CustomProgressDialog(Register.this,
					"Authenticating...");
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				CallRegistrationApi reg = new CallRegistrationApi();
				return reg.registation(name, address, phoneno, postcode, username, password, email);
			} catch (Exception e) {
				return null;
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try
			{
				JSONObject jResult = new JSONObject(result);
			
				if(jResult.getString("Success").equalsIgnoreCase("true"))
				{
					ConfigMain.Address=jResult.getString("Address");
					ConfigMain.CustomerId=jResult.getString("CustomerId");
					ConfigMain.CustomerName=jResult.getString("CustomerName");
					ConfigMain.Password=jResult.getString("Password");
					ConfigMain.PostCode=jResult.getString("PostCode");
					ConfigMain.PhoneNumber=jResult.getString("PhoneNo");
					ConfigMain.Username=jResult.getString("UserName");
					ConfigMain.Email=jResult.getString("Email");
					
					startActivity(new Intent(ctx,Login.class));
					finish();
				}
				else
				{
					Toast.makeText(getBaseContext(), "Registration Failed..", Toast.LENGTH_SHORT).show();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			dialog.dismiss();
		}
	}
}