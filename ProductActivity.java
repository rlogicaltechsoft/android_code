package com.app.homedelivery.menu;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.homedelivery.AnimatedActivity;
import com.app.homedelivery.R;
import com.app.homedelivery.TabHostActivity;
import com.app.homedelivery.Util.ConfigMain;
import com.app.homedelivery.bean.AddToCartBean;
import com.app.homedelivery.bean.ProductBean;

public class ProductActivity extends Activity {

	Context ctx;
	
	ListView lv_product;
	ImageView btn_back;
	String category_id = "";
	ArrayList<ProductBean> allProduct = new ArrayList<ProductBean>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.product);
		
		ctx = this;
		
		//Intent ii = getIntent();
		//category_id = ii.getStringExtra("categoryId");
		category_id = ConfigMain.categoryID;
		lv_product = (ListView) findViewById(R.id.lv_product);
		
		btn_back = (ImageView) findViewById(R.id.btn_back_vieworder);
		btn_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AnimatedActivity pActivity = (AnimatedActivity)ProductActivity.this.getParent();                    
		        Intent intent = new Intent(ProductActivity.this, CategoryActivity.class);
		        pActivity.finishChildActivity(intent);
			}
		});
		
		new asyncProductData().execute();
	}
	
	@Override
	public void onBackPressed() { 
		AnimatedActivity pActivity = (AnimatedActivity)ProductActivity.this.getParent();                    
        Intent intent = new Intent(ProductActivity.this, CategoryActivity.class);
        pActivity.finishChildActivity(intent);
	}

	public class asyncProductData extends AsyncTask<Void, Void, String> {
		public asyncProductData() {
		
		}

		protected String doInBackground(Void... params) {
			return null;
		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				
					allProduct = new ArrayList<ProductBean>();
					
					for (int i = 0; i < ConfigMain.JProduct.length(); i++) {
						JSONObject jdata = ConfigMain.JProduct.getJSONObject(i);
						
						if(jdata.getString("CategoryId").equals(category_id)){
							ProductBean product = new ProductBean();
							
							product.setItemId(jdata.getString("ItemId"));
							product.setItemName(jdata.getString("ItemName"));
							product.setPrice(jdata.getString("Price"));
							product.setItemSaleFormatId(jdata.getString("ItemSaleFormatId"));
							product.setPromotionCount(jdata.getString("PromotionCount"));
							product.setFotmat(jdata.getString("Format"));
							
							allProduct.add(product);
							
						}
					}
					
					lv_product.setAdapter(new AdapterProduct());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//Adapter Category
		public class AdapterProduct extends BaseAdapter {

			private LayoutInflater mInflater;

			public AdapterProduct() {
				mInflater = (LayoutInflater) ctx
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}

			@Override
			public int getCount() {
				return allProduct.size();
			}

			@Override
			public Object getItem(int position) {
				return 0;
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@SuppressLint("NewApi")
			@Override
			public View getView(int position, View vi, ViewGroup parent) {
				if (vi == null) {
					vi = mInflater.inflate(R.layout.item_product, null);
				}

				LinearLayout ll_item_product = (LinearLayout) vi.findViewById(R.id.ll_item_product);
				
				TextView txt_item_categoryname = (TextView) vi.findViewById(R.id.txt_item_productname);
				TextView txt_item_productprice = (TextView) vi.findViewById(R.id.txt_item_productprice);
				
				final DecimalFormat money = new DecimalFormat("0.00");
				money.setRoundingMode(RoundingMode.UP);
				
				ProductBean productData = allProduct.get(position);
				
				txt_item_productprice.setText("� " +money.format(new Double(productData.getPrice())));
				if(!productData.getFotmat().equals("")){
					txt_item_categoryname.setText(productData.getItemName() + " (" + productData.getFotmat()+")");
				}else{
					txt_item_categoryname.setText(productData.getItemName());
				}
				
				
				txt_item_productprice.setTypeface(ConfigMain.typeFace_REG);
				txt_item_categoryname.setTypeface(ConfigMain.typeFace_REG);

				ll_item_product.setTag(position);
				
				ll_item_product.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						ProductBean productCartData = allProduct.get(Integer.parseInt(v.getTag().toString()));
						
						double DiQty = Double.parseDouble("1");
						double diprice = (Double.parseDouble(productCartData.getPrice()));

						double total = (DiQty * diprice);
						
						String ItemRowId1 = "" + ((int) (Math.random() * 9999) + 1000);
						
						AddToCartBean bean = new AddToCartBean(productCartData.getItemId(),
								productCartData.getItemName(),
								"",
								ConfigMain.categoryID,
								"1",
								productCartData.getPrice(),
								"0",
								String.valueOf(total),
								productCartData.getPromotionCount(), 
								productCartData.getItemSaleFormatId(),
								"0",
								ItemRowId1, "0", "0", "0", "", "0");

						ConfigMain.mlistAddtoCart.add(bean);
						
						toastCustom();
						
						displayNotification();
						
						
					}
				});
				
				return vi;
			}
		}
	}
	
	// Get Product Qty Count
	public int qtyTotal() {
		int aQty = 0;
		for (int i = 0; i < ConfigMain.mlistAddtoCart.size(); i++) {
			aQty += Double.parseDouble(ConfigMain.mlistAddtoCart.get(i).getQty().toString());
		}
				
		return aQty;
	}
	
	// Display Notification Count
	public void displayNotification(){
		if(qtyTotal() == 0){
			TabHostActivity.txt_cart_count.setVisibility(View.INVISIBLE);
		}else{
			TabHostActivity.txt_cart_count.setVisibility(View.VISIBLE);
				
			if(qtyTotal() > 9){
				TabHostActivity.txt_cart_count.setText(""+ qtyTotal());
			}else{
				TabHostActivity.txt_cart_count.setText("0"+ qtyTotal());
			}
		}
	}
		
	// My custom toast
	public void toastCustom() {
		Context context=getApplicationContext();
		LayoutInflater inflater=getLayoutInflater();
		
		View customToastroot =inflater.inflate(R.layout.mytoast, null);
				
		Toast customtoast=new Toast(context);
				
		customtoast.setView(customToastroot);
		customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,0, 0);
		customtoast.setDuration(1000);
		customtoast.show();
	}
	
}
