package com.rlogical.restaurant;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rlogical.restaurant.Util.ConfigMain;
import com.rlogical.restaurant.Util.Utility;
import com.rlogical.restaurant.api.LoginApi;
import com.rlogical.restaurant.widget.CustomProgressDialog;

public class Login extends Activity {
	Context ctx;
	
	LinearLayout ll_login;
	EditText txt_login_password;
	Button btn_login;
	LinearLayout.LayoutParams parms;
	
	TextView txt_splace_headername;
	Typeface typeFace_header;
	
	// Preference
	public static final String PREFS_NAME = "RESTAURANT_ANDROID";
	SharedPreferences settings;
	Editor editor;
	
	Dialog dialog_ServerSetting;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		
		setContentView(R.layout.login);
		
		

		ctx = this;

		Utility.hideKeyboard(Login.this);
		
		/*if(!settings.contains("EULA")) {
			showLicenseAgreement();
		}*/

		ll_login = (LinearLayout) findViewById(R.id.ll_login);
		
		txt_splace_headername = (TextView) findViewById(R.id.txt_splace_headername);
		typeFace_header = Typeface.createFromAsset(getAssets(),
				"LCALLIG.TTF");

		txt_splace_headername.setTypeface(typeFace_header,Typeface.BOLD_ITALIC);
	
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			if (Utility.isTablet(ctx)) {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceWidth(this) / 1.5),
						LayoutParams.WRAP_CONTENT);
			} else {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceWidth(this) / 1.2),
						LayoutParams.WRAP_CONTENT);
			}
		} else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if (Utility.isTablet(ctx)) {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceHeight(this) / 1.5),
						LayoutParams.WRAP_CONTENT);
			} else {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceHeight(this) / 1.2),
						LayoutParams.WRAP_CONTENT);
			}
		}
		
		ll_login.setLayoutParams(parms);

		txt_login_password = (EditText) findViewById(R.id.txt_login_password);

		ConfigMain.isProduct = false;
		ConfigMain.jResult_product = new JSONArray();
		new AsyncProduct("0").execute();
		
		btn_login = (Button) findViewById(R.id.btn_login);
		btn_login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Utility.isInternetAvailable(ctx)) {
					if (txt_login_password.getText().toString().trim().length() == 0) {
						Toast.makeText(ctx, "Please Enter Password",
								Toast.LENGTH_SHORT).show();
					} else {
						new AsyncLogin(txt_login_password.getText().toString()
								.trim()).execute();
					}
				} else {
					Toast.makeText(
							ctx,
							"Something Going Wrong with your Internet Connection!!",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	// Async Login Api
	public class AsyncLogin extends AsyncTask<Void, Void, String> {
		String password;
		CustomProgressDialog dialog;

		public AsyncLogin(String pass) {
			
			dialog = new CustomProgressDialog(Login.this, "Authenticating...");
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.show();
			
			password = pass;
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				LoginApi loginapi = new LoginApi();
				return loginapi.login(password);

			} catch (Exception e) {
				return null;
			}
		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result != null) {
					JSONObject jObj = new JSONObject(result.toString());

					if (jObj.getString("Success").equalsIgnoreCase("True")) {
						ConfigMain.EmployeeId = jObj.getString("EmployeeId");
						ConfigMain.EmployeeName = jObj.getString("EmployeeName");
						ConfigMain.EmployeePassword = jObj.getString("EmployeePassword");
						ConfigMain.EmployeeProfileName = jObj.getString("ProfileName");
						ConfigMain.PriceListId = jObj.getString("PriceListId");
						
						/*ConfigMain.savePreferences(getBaseContext(), "EmployeeId", ConfigMain.EmployeeId);
						ConfigMain.savePreferences(getBaseContext(), "EmployeeName", ConfigMain.EmployeeName);
						ConfigMain.savePreferences(getBaseContext(), "EmployeePassword", ConfigMain.EmployeePassword);
						ConfigMain.savePreferences(getBaseContext(), "EmployeeProfileName", ConfigMain.EmployeeProfileName);
						*/
						
						//ConfigMain.jResult_product = new JSONArray();
						//ConfigMain.jResult_product_main = new JSONArray();
						ConfigMain.jResult_Category = new JSONArray();
						
						//ConfigMain.isProduct = false;
						ConfigMain.isCategory = false;
						
						Intent i = new Intent(getBaseContext(), TablesActivity.class);
						startActivity(i);
						finish();
						
						txt_login_password.setText("");
					} else {
						Toast.makeText(getBaseContext(),
								"Authentication Fail..", Toast.LENGTH_SHORT)
								.show();
					}
				}
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), "Authentication Fail..",
						Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			dialog.dismiss();
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			if (Utility.isTablet(ctx)) {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceWidth(this) / 1.5),
						LayoutParams.WRAP_CONTENT);
			} else {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceWidth(this) / 1.2),
						LayoutParams.WRAP_CONTENT);
			}
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if (Utility.isTablet(ctx)) {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceHeight(this) / 1.8),
						LayoutParams.WRAP_CONTENT);
			} else {
				parms = new LinearLayout.LayoutParams(
						(int) (Utility.getDeviceHeight(this) / 1.8),
						LayoutParams.WRAP_CONTENT);
			}
		}
	}
	
	public void showLicenseAgreement() {
		dialog_ServerSetting = new Dialog(Login.this);
		dialog_ServerSetting.requestWindowFeature(Window.FEATURE_NO_TITLE);    
		dialog_ServerSetting.setContentView(R.layout.server_setting);
		dialog_ServerSetting.setCancelable(false);
	
		dialog_ServerSetting.show();
		
	}
}