package com.rlogical.restaurant.bean;

public class TablesBean {

	String RoomId;
	String TableNo;
	String IsBusy;
	String TicketId;
	
	public TablesBean() {
		super();
	}

	public TablesBean(String tableNo, String isBusy, String TicketId) {
		super();
		TableNo = tableNo;
		IsBusy = isBusy;
		this.TicketId = TicketId;
	}

	public String getTicketId() {
		return TicketId;
	}

	public void setTicketId(String ticketId) {
		TicketId = ticketId;
	}

	public String getRoomId() {
		return RoomId;
	}

	public String getIsBusy() {
		return IsBusy;
	}

	public void setIsBusy(String isBusy) {
		IsBusy = isBusy;
	}

	public void setRoomId(String roomId) {
		RoomId = roomId;
	}

	public String getTableNo() {
		return TableNo;
	}

	public void setTableNo(String tableNo) {
		TableNo = tableNo;
	}

}
